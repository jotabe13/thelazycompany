package Ejercico3;


import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static int i=0;

    public static void main(String[] args) {
        //Main. Aca llamo las funciones para imprimir y cargar personas
        imprimirMatriz(cargaPersona());

    }

    public static String[][] cargaPersona(){
        //Declaro las variables a utilizar y creo un objeto persona
        Persona persona1 = new Persona();
        boolean bandera=true,bandera2=true;
        String matriz[][]=new String[10][3];
        String usuario;
        Scanner sc = new Scanner(System.in);

        //Do-while para la carga de personas
        do {
            bandera2=true;
            System.out.println("Ingrese el nombre: ");
            persona1.setNombre(sc.next());
            System.out.println("Ingrese el dni: ");
            persona1.setDni(sc.next());
            System.out.println("Ingrese la edad: ");
            persona1.setEdad(sc.next());
            matriz[i][0]=persona1.getNombre();
            matriz[i][1]=persona1.getDni();
            matriz[i][2]=persona1.getEdad();





                //While para el control de lo que ingresa el usuario luego de la pregunta
               while(bandera2){
                   System.out.println("Desea seguir cargando personas? Ingrese la palabra 'Si' o la palabra 'No'"); //Pregunto si quiere seguir cargando personas
                   usuario = sc.next();
                    if (usuario.equals("No") || usuario.equals("no")) {    //Si el usuario responde negativamente, se cambian las banderas y se sale de los dos bucles
                       bandera = false;
                       bandera2=false;
                   } else if(usuario.equals("Si") || usuario.equals("si")){ //Si el usuario responde positivamente se suma la variable global i (contador de personas) y se sale del bucle de control
                       i++;
                       bandera2=false;
                   } else {
                        System.out.println("Se ha ingresado una palabra erronea. Intente de nuevo"); //Se imprime un mensaje de error cuando el usuario ingresa otra cosa
                        continue;
                    }

               }


        }while (bandera);
        return matriz;
    }
    //Funcion para poder imprimir la matriz
    public static void imprimirMatriz(String matriz[][]){
        for(int j=0;j<=i;j++){
            if(matriz[j][0].equals("null")){ //Esto es para evitar que se impriman lugares de la matriz que estan en null.
                break;
            }else{
                System.out.println("Nombre: "+matriz[j][0]);
                System.out.println("DNI: "+matriz[j][1]);
                System.out.println("Edad: "+matriz[j][2]);
                System.out.print("\n");
            }


        }
    }






}
