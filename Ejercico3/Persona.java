package Ejercico3;

public class Persona {
    private String nombre, dni,edad;
    public Persona(){
        this.nombre="Juan";
        this.edad="21";
        this.dni="41516512";
    }
    public Persona(String nombre,String dni, String edad){
        this.nombre=nombre;
        this.dni=dni;
        this.edad=edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }





}
