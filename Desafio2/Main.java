package Desafio2;
import java.util.Scanner;

public class Main extends Facultad {
    public static void main(String[] args) {



        //Instancias
        Scanner sc = new Scanner(System.in);
        Facultad facultad = new Facultad("UTN");
        Profesores facundoUferer = new Profesores(20000,2,"Facundo","Uferer", "30032");
        Profesores carina = new Profesores(35000,2,"Carina","Jovanovich", "30032");
        Profesores matoff = new Profesores(100000,2,"Facundo","Matoff", "30032");
        Materia matematica = new Materia("Matematicas",carina);
        Materia laboratorio = new Materia("Laboratorio",facundoUferer);
        Materia programacion = new Materia("Programacion",matoff);
        Carrera prog =  new Carrera("Programacion");
        Carrera ing =  new Carrera("Ingenieria en Sistemas");
        Estudiante juan= new Estudiante("Juan ","Burich", "30021");
        Estudiante manu= new Estudiante("Manuel Galileo ","Alonso", "30021");
        Estudiante pablito= new Estudiante("Pablo ","Sanchez", "30021");
        Estudiante litch= new Estudiante("Luciano ","Chichari", "30021");
        Estudiante pablito2= new Estudiante("Pablo ","Gallardo", "30021");

        /*Ejecucion*/
        //AGREGAR CARRERAS

        facultad.agregarCarreras(prog);
        facultad.agregarCarreras(ing);
        facultad.listarContenidos();
        System.out.println();

        //ELIMINAR CARRERAS
        facultad.eliminarCarrreas(prog);
        System.out.println();

        //AGREGAR MATERIAS
        prog.agregarMateria(laboratorio);
        prog.agregarMateria(matematica);
        prog.agregarMateria(programacion);
        prog.listarContenidos();
        prog.eliminarMateria("Laboratorio");
        System.out.println();

        //ENCONTRAR MATERIAS
        prog.encontrarMateria("Matematicas");
        System.out.println();

        //AGREGAR ESTUDIANTES
        matematica.agregarEstudiantes(juan);
        matematica.agregarEstudiantes(manu);
        matematica.agregarEstudiantes(litch);
        matematica.agregarEstudiantes(pablito);
        matematica.agregarEstudiantes(pablito2);
        System.out.println();


        //ELIMINAR ESTUDIANTES
        matematica.eliminarEstudiantes("Sanchez");
        System.out.println();
        System.out.println("Alumnos!");
        matematica.listarContenidos();
        System.out.println();

        //MODIFICAR TITULAR
        matematica.modificarTitular(facundoUferer);
        System.out.println();

        //CALCULAR SUELDO
        System.out.println("SUELDOS!");
        facundoUferer.calcularSueldo();
        carina.calcularSueldo();
        matoff.calcularSueldo();
        System.out.println();

        //LISTAR CONTENIDOS
        facultad.listarContenidos();
        System.out.println();
        prog.listarContenidos();
        System.out.println();




    }
}
