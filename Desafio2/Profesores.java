package Desafio2;

public class Profesores extends Persona {
    private double basico;
    private int antiguedad;



    //Constructor
    public Profesores(double basico, int antiguedad,String nombre, String apellido, String legajo) {
        this.nombre=nombre;
        this.apellido=apellido;
        this.legajo=legajo;
        this.basico = basico;
        this.antiguedad = antiguedad;
    }

    public Profesores(){

    }

    //Getters y Setters
    public double getBasico() {
        return basico;
    }

    public void setBasico(double basico) {
        this.basico = basico;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }



    //Metodo
    public double calcularSueldo() {
        double sueldo=basico+((basico*0.10)*antiguedad);
        System.out.println("$"+sueldo);
        return sueldo;
    }

    @Override
    public String toString() {
        return "Profesores{" +
                "basico=" + basico +
                ", antiguedad=" + antiguedad +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", legajo='" + legajo + '\'' +
                '}';
    }
}
