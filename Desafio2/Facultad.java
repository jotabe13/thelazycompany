package Desafio2;

import java.util.*;

public class Facultad extends Carrera implements Informacion {
    private String nombre;
    private String carrera;
    protected List<Carrera> coleccionCarreras = new ArrayList<>();

    //Constructor
    public Facultad(){

    }
    public Facultad(String nombre) {
        this.nombre = nombre;
    }


    //Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    //Metodos
    public void agregarCarreras(Carrera carrera){
        coleccionCarreras.add(carrera);

    }
    public void eliminarCarrreas(Carrera carrera) {
        Iterator<Carrera> it=coleccionCarreras.iterator();
        while (it.hasNext()){
            if(it.next().equals(carrera)){
                it.remove();
                System.out.println("La carrera "+carrera.getNombre()+" se elimino con exito!");
            }
        }
    }

    public class NameSorter implements Comparator<Carrera>
    {
        @Override
        public int compare(Carrera o1, Carrera o2) {
            return o2.getNombre().compareToIgnoreCase(o1.getNombre());
        }
    }

    @Override
    public int verCantidad() {
        return coleccionCarreras.size();
    }


    @Override
    public String listarContenidos() {
        System.out.println("Carreras actuales!");
        coleccionCarreras.sort(new NameSorter().reversed());
        Iterator<Carrera> it=coleccionCarreras.iterator();
        while (it.hasNext()){
            String carrera=it.next().getNombre();
            System.out.println(carrera);
        }
        return carrera ;
    }

    @Override
    public String toString() {
        return "Facultad{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
