package Desafio2;

import java.util.*;

public class Materia extends Estudiante implements Informacion,Comparable<Estudiante>{
    private String nombre;
    private String estudiante;
    private Profesores titular;
    ArrayList<Estudiante> coleccionEstudiante = new ArrayList<>();

    //Constructor
    public Materia(String nombre, Profesores titular) {
        this.nombre = nombre;
        this.titular = titular;
    }
    public Materia(){

    }

    //Getters y Setter
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Profesores getTitular() {
        return titular;
    }

    public void setTitular(Profesores titular) {
        this.titular = titular;
    }


    //Metodos
    public void agregarEstudiantes(Estudiante estudiante){
        coleccionEstudiante.add(estudiante);
        System.out.println("Estudiante "+estudiante.nombre + estudiante.apellido+" agregado con exito!");

    }

    public void eliminarEstudiantes(String nombre){
        Iterator<Estudiante> it=coleccionEstudiante.iterator();
        while (it.hasNext()){
            if(it.next().apellido.equals(nombre)){
               it.remove();
               System.out.println("El estudiante "+nombre+" se elimino con exito!");
            }
        }
    }

    public void modificarTitular(Profesores profesor ){
        setTitular(profesor);
        System.out.println("Profesor actualizado con exito! El nuevo profesor es: "+profesor.nombre+" "+profesor.apellido );
    }

    public class NameSorter implements Comparator<Estudiante>
    {
        @Override
        public int compare(Estudiante o1, Estudiante o2) {
            return o2.apellido.compareToIgnoreCase(o1.apellido);
        }
    }

    @Override
    public String toString() {
        return "Materia{" +
                "nombre='" + nombre + '\'' +
                ", titular=" + titular +
                '}';
    }

    @Override
    public int verCantidad() {
        return coleccionEstudiante.size();
    }

    @Override
    public String listarContenidos() {
        coleccionEstudiante.sort(new NameSorter().reversed());
        Iterator<Estudiante> it = coleccionEstudiante.iterator();
        while (it.hasNext()){
            String estudiante=it.next().apellido;
            System.out.println(estudiante);
        }
        return estudiante ;
    }

    @Override
    public int compareTo(Estudiante o) {
        return this.apellido.compareTo(o.apellido);
    }
}
