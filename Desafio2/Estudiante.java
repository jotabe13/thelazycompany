package Desafio2;

public class Estudiante extends Persona {

    public Estudiante(String nombre, String apellido, String legajo){
      this.nombre=nombre;
      this.apellido=apellido;
      this.legajo=legajo;
    }

    public Estudiante(){

    }



    @Override
    public String toString() {
        return "Estudiante{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", legajo='" + legajo + '\'' +
                '}'+ '\n';
    }
}
