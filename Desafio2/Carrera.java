package Desafio2;

import java.util.*;

public class Carrera extends Materia implements Informacion{
    private String nombre;
    private String materia;
    ArrayList <Materia> coleccionMaterias = new ArrayList<>();

    //Constructor
    public Carrera (String nombre){
        this.nombre = nombre;
    }
    public Carrera(){ }

    //Getter and Setter
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //Metodos
    public void agregarMateria (Materia materia){
        coleccionMaterias.add(materia);

    }


    public void eliminarMateria (String materia){
        Iterator<Materia> it=coleccionMaterias.iterator();
        while(it.hasNext()){
            if(it.next().getNombre().equals(materia)){
                it.remove();
                System.out.println("La materia "+materia+" se elimino con exito");
            }
        }
    }


    public void encontrarMateria (String nombre){
        String usuario="";
        Scanner sc = new Scanner(System.in);
        Iterator<Materia> it = coleccionMaterias.iterator();
        while (it.hasNext()){
            if(it.next().getNombre().equals(nombre)){
                System.out.println("Materia "+nombre+" encontrada! Desea eliminarla? Si/No");
                usuario=sc.next();
                if(usuario.equals("si") || usuario.equals("Si") || usuario.equals("SI")){
                    it.remove();
                    System.out.println("Materia ELIMINADA!");
                }
                else {
                    System.out.println("La materia no ha sido eliminada!");
                }
            }
        }
    }

    public class NameSorter implements Comparator<Materia>
    {
        @Override
        public int compare(Materia o1, Materia o2) {
            return o2.getNombre().compareToIgnoreCase(o1.getNombre());
        }
    }

    @Override
    public String listarContenidos() {
        System.out.println("Materias actuales!");
        coleccionMaterias.sort(new NameSorter().reversed());
        Iterator<Materia> it = coleccionMaterias.iterator();
        while (it.hasNext()){
            String materia=it.next().getNombre();
            System.out.println(materia);
        }
        return materia ;
    }

    @Override
    public String toString() {
        return "Carrera{" +
                "nombre='" + nombre + '\'' +
                '}'+'\n';
    }
}
