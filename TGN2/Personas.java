package TGN2;
public abstract class  Personas {
    private String nombre;
    private int edad;


    //Constructor
    public Personas (String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }


    //Getter and Setter
    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}
    public int getEdad() {return edad;}
    public void setEdad(int edad) {this.edad = edad;}


    //METODOS
    public abstract String getTipo();
    public abstract String toString();
}
