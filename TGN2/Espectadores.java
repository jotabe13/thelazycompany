package TGN2;


public class Espectadores extends Personas{
    private char fila;
    private int silla;

    //constructor
    public Espectadores(String nombre, int edad, char fila, int silla){
        super(nombre, edad);
        this.fila = fila;
        this.silla = silla;
    }

    //getter and setter
    public char getFila() {return fila;}
    public void setFila(char fila) {this.fila = fila;}
    public int getSilla() {return silla;}
    public void setSilla(int silla) {this.silla = silla;}


    @Override
    public void setEdad(int edad) {super.setEdad(edad);}
    @Override
    public void setNombre(String nombre) {super.setNombre(nombre);}



    //Metodos
    public void getButaca(){
        System.out.println("El espectador "+this.getNombre()+" se encuentra");
        System.out.println("En la fila: "+getFila());
        System.out.println("En la silla: "+getSilla()+"\n");
    }
    @Override
    public String  getTipo(){
        return "Espectador";
    }
    @Override
    public String toString() {
        return "Nombre: "+ getNombre()+"\n" + "ES: "+this.getTipo()+"\n";
    }
}
