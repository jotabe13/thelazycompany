package TGN2;
import java.util.Scanner;
public class Acomodadores extends Empleados implements ParaAcomodadores{
    private String salas;

    //Constructor
    public Acomodadores (String nombre, int edad, String salas){
        super(nombre, edad);
        this.salas = salas;
    }
    //getter and setter
    public String getSalas() {return salas;}
    public void setSalas(String salas) {this.salas = salas;}

    //Metodos
    public void asignacionSala(Salas sala){
        setSala(sala);
        System.out.println(sala.getNombre()+ " agregada al acomodador " + getNombre()+"\n");
    }

    public void modificarSala(Salas sala){
        Scanner sc = new Scanner(System.in);
        String bandera;
        do {
            System.out.println("¿Que desea modificar?" +"\n"+
                    "Nombre \n" +
                    "Capacidad \n" +
                    "Pelicula \n" +
                    "Opcion: ");
            String usuario=sc.next();

            switch (usuario){
                case "Nombre":
                    System.out.println("¿Que nombre desea colocarle?");
                    usuario=sc.next();
                    sala.setNombre(usuario);
                    System.out.println(sala.getNombre());
                    break;
                case "Capacidad":
                    System.out.println("¿Que capacidad desea colocarle?");
                    int cap=sc.nextInt();
                    sala.setCapacidad(cap);
                    System.out.println(sala.getCapacidad());
                    break;
                case "Pelicula":
                    System.out.println("¿Que pelicula desea colocarle?");
                    String pelicula=sc.next();
                    sala.setPelicula(pelicula);
                    System.out.println(sala.getPelicula());
                    break;
                default:
                    System.out.println("ATENCION! Opcion incorrecta!");
            }
            System.out.println("¿Desea seguir modificando?");
            bandera=sc.next();
        }while(bandera.equals("Si") || bandera.equals("si") || bandera.equals("SI"));
    }
    @Override
    public String toString() {
        return "Datos Acomodador: "+"\n" + "Nombre: " + super.getNombre() + '\n' + "Sala asignada: " + salas + '\n' + "Edad: " + super.getEdad()+"\n" + "ES: "+this.getTipo()+"\n";
    }
    @Override
    public String  getTipo(){
        return "Acomodador";
    }
    @Override
    public Salas getSala() {
        return null;
    }

    @Override
    public void setSala(Salas sala) {}
}
