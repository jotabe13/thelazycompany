package TGN2;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;

public class Empleados extends Personas {
    private double sueldo = 40000.50;
    private Set<Espectadores> totalEspectadores = new HashSet<Espectadores>();


    //Constructor
    public Empleados (String nombre, int edad, double sueldo){
        super(nombre, edad);
        this.sueldo = sueldo;
    }
    public Empleados(String nombre, int edad) {
        super(nombre, edad);
    }

    //getter and setter
    public double getSueldo() {return sueldo;}
    public void setSueldo(double nuevoSueldo) {
        this.sueldo = nuevoSueldo;
    }


    public Espectadores crearEspectadores(){
        Scanner sc = new Scanner(System.in);
        String nombre,usuario;
        char fila;
        int edad,silla;
        try {
            System.out.println("Agregar espectadores: ");
            System.out.println("Nombre: ");
            nombre = sc.next();
            System.out.println("Edad: ");
            edad = sc.nextInt();
            System.out.println("Fila: ");
            fila = sc.next().charAt(0);
            System.out.println("Silla: ");
            silla = sc.nextInt();
            return new Espectadores(nombre, edad, fila, silla);
        }
        catch (Exception e){
            System.out.println("ATENCION! Error en el ingreso de datos! --> Codigo de error: "+e.toString());
            return null;
        }
    }

    //Metodos
    @Override
    public String  getTipo(){
        return "Empleado";
    }
    @Override
    public String toString() {
        return "Datos Empleado: "+"\n" + "Nombre: " + super.getNombre() + '\n' + "Edad: " + super.getEdad()+"\n" + "ES: "+this.getTipo()+"\n";
    }
}
