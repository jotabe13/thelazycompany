package TGN2;


import java.util.Scanner;

public class Cine {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String usuario;
        Scanner num = new Scanner(System.in);
        double nuevoSueldo;

        //INSTANCIAS
        Salas sala1 = new Salas(3,"Sala 1", "Joker");

        //CREAR UN EMPLEADO
        Empleados empleado1 = new Empleados("Juan", 23,30000);
        Acomodadores pedro = new Acomodadores("Pedro",25,"Sala 2");
        Espectadores juan = new Espectadores("Juan Segundo",19,'A',3);
        Espectadores espectador;

        do {
            espectador=empleado1.crearEspectadores();
            sala1.agregarEspectadores(espectador);
            System.out.println("Desea seguir agregando espectadores?");
            usuario=sc.next();
            sala1.listarEspectadores();
        }while (usuario.equals("si") || usuario.equals("Si") || usuario.equals("SI"));


        //DATOS ACOMODADOR POR CONSOLA
        System.out.println(pedro.toString());

        //MOSTRAR LOS DATOS DEL EMPLEADO
        System.out.println(empleado1.toString());

        //MODIFICAR SUELDO EMPLEADO
        System.out.println("Sueldo anterior: "+empleado1.getSueldo());
        System.out.println("Ingrese el nuevo sueldo del empleado: ");
        try{
            nuevoSueldo = num.nextDouble();
            empleado1.setSueldo(nuevoSueldo);
            System.out.println("Sueldo actualizado: "+empleado1.getSueldo()+"\n");
        }
        catch (Exception e){
            System.out.println("ATENCION! El tipo de dato no es valido --> Codigo de error: "+e.toString());
        }


        //getTipo abstracto
        System.out.println(juan.toString());
        espectador.getButaca();

        //ASIGNACION DE SALA A ACOMODADOR
        pedro.asignacionSala(sala1);

        //MODIFICACION DE SALA
        pedro.modificarSala(sala1);
    }
}
